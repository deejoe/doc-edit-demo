I'm going to throw some mermaid in here I guess?

This is like the mermaid `hello, world`:

    ```mermaid
    graph TD;
      A-->B;
      A-->C;
      B-->D;
      C-->D;
    ```

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

